

# Requirements

- nvm
- git

# Installation

```
git clone git@gitlab.com:salih.four/foss-meet-2020-website.git fossmeet2020

cd fossmeet2020

nvm install 12

npm install
```

# Development

```
npm start
```

## Design Elements

<a>https://drive.google.com/drive/folders/1l7g_GBPZRiCVupKnd7Ma5e0LENuOmSJN</a>
